#include <algorithm>
#include <string.h>
#include <iostream>
#include <algorithm>

int main(int argc, char **argv) {
	std::vector<std::string> args(argv + 1, argv + argc);
	std::string infname, outfname;
	bool n = 0, r = 0, l = 0;
	for (auto i = args.begin(); i != args.end(); ++i) {
		if (*i == "-n")
			n = 1;
		else if (*i == "-r")
			r = 1;
		else if (*i == "-l")
			l = 1;
	}

	if (r)
		std::reverse(args.begin(), args.end());

	for (auto i = args.begin(); i != args.end(); ++i)
		std::cout << *i << (l ? "\n" : " ");
	if (!n)
		std::cout << std::endl;
}
