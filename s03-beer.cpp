#include <iostream>
#include <string.h>

int main(int argc, char **argv) {
	int beers = 99;
	if (argc > 1)
		beers = std::atoi(argv[1]);

	for (int i = beers; i > 1; --i) {
		std::cout << i << " bottles of beer on the wall, " << i
				<< " bottles of beer.\n" << "Take one down, pass it around, "
				<< i << " bottles of beer on the wall...\n";
	}
	if (beers > 0) {
		std::cout << "1 bottle of beer on the wall, 1 bottle of beer.\n";
		std::cout << "Take one down, pass it around\n";
	}
	std::cout
			<< "No more bottles of beer on the wall, no more bottles of beer.\n";
	std::cout << "Go to the store and buy some more, " << beers
			<< " bottles of beer on the wall...";
	return 0;
}
