#include <iostream>
#include<string.h>

int main(int argc, char **argv) {
	if (argc < 2)
		return 1;
	const int MOD = std::atoi(argv[1]);
	for (int i = 1; i <= MOD; ++i) {
		std::cout << i << " ";
		if (i % 3 == 0)
			std::cout << "Fizz";
		if (i % 5 == 0)
			std::cout << "Buzz";
		std::cout << "\n" ;
	}
	return 0;
}
