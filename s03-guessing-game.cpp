#include <iostream>
#include <string.h>
#include <random>
#include <cctype>

int rand1to100() {
	std::random_device os_seed;
	const uint_least32_t seed = os_seed();
	std::mt19937 generator(seed);

	std::uniform_int_distribution<uint_least32_t> distribute(1, 100);
	return distribute(generator);
}

int main(int argc, char **argv) {
	int randomNumber = rand1to100();
	std::string guess;

	while (1 == 1) {
		std::getline(std::cin, guess);
		if (randomNumber != std::stoi(guess))
			break;
		std::cout
				<< (randomNumber > std::stoi(guess) ?
						"za mała liczba" : "za duża liczba") << std::endl;
	}
	std::cout << "dobra liczba!" << std::endl;

	return 0;
}
