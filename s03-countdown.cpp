#include <iostream>
#include<string.h>

int main(int argc, char **argv) {
	if (argc < 2)
		return 1;

	for (int i = std::atoi(argv[1]); i >= 0; --i)
		std::cout << i << "...\n";

	return 0;
}
